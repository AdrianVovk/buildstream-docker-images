# Makefile for managing BuildStream-related Docker images.
#

SHELL=/bin/bash

bytes_to_mb = $(shell echo $$(( $(1) / 1024 / 1024 )))
image_size = $(shell docker image inspect --format '{{.Size}}' $(1))
image_size_mb = $(call bytes_to_mb,$(call image_size,$(1)))

# Overridable variables
BUILD_ARGS ?=
CONTEXT ?= $(CURDIR)
DOCKERFILE ?= $(CONTEXT)/Dockerfile
TOXENV ?=

# Dependencies
BASE_DEPS = git shellcheck
PYTHON_DEPS = black jinja2 pyyaml yamllint
HADOLINT_VERSION = v1.17.5
HADOLINT_URL = "https://github.com/hadolint/hadolint/releases/download/$(HADOLINT_VERSION)/hadolint-Linux-x86_64"

# Commands
BLACK = black --line-length 79
# ignore version pinning and files not included
HADOLINT = hadolint --ignore DL3008 --ignore DL3013 --ignore SC1091
YAMLLINT = yamllint --strict
WHITESPACE_CHECK = grep -En ' +$$'
BST_DOCKER_RUN = docker run --rm --privileged --device /dev/fuse --env TOXENV=$(TOXENV)
BST_TEST = git clone https://github.com/apache/buildstream.git && cd buildstream && tox -- --color=yes --integration
BST_SMOKE_TEST = bst --version
BUILDBOX_CASD_SMOKE_TEST = buildbox-casd --help

# Targets
TARGETS_DOCKER = build push release
TARGETS_LINT = black-check shellcheck hadolint yamllint whitespace-check
TARGETS_FORMAT = black-fmt render
TARGETS_CI = ci-image-compare ci-image-pull ci-install-deps ci-static-checks ci-template-check
TARGETS_TEST = bst-test

.PHONY: all $(TARGETS_DOCKER) $(TARGETS_LINT) $(TARGETS_FORMAT) $(TARGETS_CI) $(TARGETS_TEST)

all: build

# Main Docker targets

build:
	docker build ${BUILD_ARGS} --file $(DOCKERFILE) --tag $(FIXED_TAG) $(CONTEXT)

push:
	docker push $(FIXED_TAG)

release:
	docker pull $(FIXED_TAG)
	docker tag $(FIXED_TAG) $(MOVING_TAG)
	docker push $(MOVING_TAG)

# Lint targets

lint: $(TARGETS_LINT)

black-check:
	find . -name '*.py' | xargs $(BLACK) --check

shellcheck:
	find . -name '*.sh' | xargs shellcheck

hadolint:
	find . -name '*Dockerfile' | xargs $(HADOLINT)

yamllint:
	find . -name '*.yml' | xargs $(YAMLLINT)

whitespace-check:
	@{ find . -name '*Dockerfile' | xargs $(WHITESPACE_CHECK); } && \
        { echo "ERROR: The above lines have trailing whitespaces."; exit 1; } || \
		{ echo "No trailing whitespaces found :)"; }

# Format targets

black-fmt:
	find . -name '*.py' | xargs $(BLACK)

render:
	templates/render.py

# Test targets

bst-test:
	$(BST_DOCKER_RUN) $(FIXED_TAG) /bin/sh -c "$(BST_TEST)"

bst-smoke-test:
	$(BST_DOCKER_RUN) $(FIXED_TAG) /bin/sh -c "$(BST_SMOKE_TEST)"

buildbox-casd-smoke-test:
	$(BST_DOCKER_RUN) $(FIXED_TAG) /bin/sh -c "$(BUILDBOX_CASD_SMOKE_TEST)"

# CI targets

ci-image-compare: ci-image-pull
	docker history $(MOVING_TAG)
	docker history $(FIXED_TAG)
	@echo
	@echo "Image size changed by $$(( $(call image_size_mb,$(MOVING_TAG)) - $(call image_size_mb,$(FIXED_TAG)) )) Mb (less is better!)".

ci-image-pull:
	docker pull $(MOVING_TAG)

ci-install-deps:
	apt-get update
	apt-get install --yes --no-install-recommends $(BASE_DEPS)
	pip3 install $(PYTHON_DEPS)
	curl -L -o /usr/local/bin/hadolint $(HADOLINT_URL)
	chmod +x /usr/local/bin/hadolint

ci-static-checks: ci-template-check lint

ci-template-check: render
	git diff --exit-code || (echo 'Templates are not up to date. Please run ./templates/render.py and commit' && exit 1)
