---
###############
## Templates ##
###############

# Defaults
#
# This is the defaults that are common between each platform
.default: &defaults

  # Default locale, should be present in most newer distributions. For older
  # distributions, this must be overriden to some UTF-8 locale.
  locale: C.UTF-8

  commands: &commands
    preinstall: []

    package_install:

    pip: pip3

    install_buildbox:
      - PROJECT=https://gitlab.com/buildgrid/buildbox/buildbox-integration
      - PERMALINK=latest/downloads/buildbox-x86_64-linux-gnu.tgz
      - curl
          --location ${PROJECT}/-/releases/permalink/${PERMALINK}
          --output ./buildbox.tar.gz
      - tar --extract --directory /usr/local/bin --file ./buildbox.tar.gz
      - rm ./buildbox.tar.gz

    postinstall: []

    cleanup: []

  dependencies: &dependencies
    buildstream:
      - bubblewrap
      - fuse3

    plugins:
      - git
      - lzip
      - patch

    external_plugins:
      - git-lfs
      - quilt

    ci:
      - findutils
      - make

  python_dependencies:
    # NOTE: We are currently incompatible with Tox 4.
    - "'tox<4'"
    - wheel

  user: testuser

  allow_unsafe_git_file_protocol: true


# RHEL templates
#
# Template with entries that are shared between every RHEL based distributions
# Mainly Fedora and CentOS
.rhel-template: &rhel
  <<: *defaults

  commands: &rhel-commands
    <<: *commands

    package_install: dnf install --assumeyes

    cleanup:
      - dnf remove --assumeyes cmake
      - dnf clean all

  dependencies: &rhel-dependencies
    <<: *dependencies

    ostree:
      - ostree

    build:
      - cairo-gobject-devel
      - gcc
      - gcc-c++
      - fuse-devel
      - gobject-introspection-devel

    python:
      - python3
      - python3-devel
      - python3-pip
      - python3-setuptools


# Fedora
#
# Template for fedora versions
.fedora-template: &fedora
  <<: *rhel


# Debian
#
# Template with entries that are shared between every Debian based distributions
# Mainly Debian and Ubuntu
.debian-template: &debian
  <<: *defaults

  env_extra: DEBIAN_FRONTEND=noninteractive

  commands: &debian-commands
    <<: *commands

    cleanup:
      - apt-get autoremove --yes
          cmake curl protobuf-compiler protobuf-compiler-grpc
      - apt-get clean
      - rm -rf /var/lib/apt/lists

    preinstall:
      - apt-get update

    package_install: apt-get install --yes --no-install-recommends

  dependencies: &debian-dependencies
    <<: *dependencies

    python:
      - python3
      - python3-dev
      - python3-pip
      - python3-setuptools
      - python3-venv

    build:
      - curl
      - libcairo2-dev
      - libgirepository1.0-dev

# Debian Buster
#
# Template based on Debian, customized for debian:10 (buster)
.buster-template: &buster
  <<: *debian

  commands:
    <<: *debian-commands

    preinstall:
      - apt-get update
      # We need a more recent ostree, let's install it
      # yamllint disable-line rule:line-length
      - apt-get install --yes --no-install-recommends ostree gir1.2-ostree-1.0


############
## Images ##
############

fedora:37:
  <<: *fedora

fedora:38:
  <<: *fedora

fedora:39:
  <<: *fedora

debian:10:
  <<: *buster


ubuntu:20.04:
  <<: *debian


# Fedora based image with minimal system dependencies
#
# This image is for testing that BuildStream behaves sensibly when sandboxing
# tools are not present.
fedora-minimal:
  <<: *fedora

  base: fedora:38

  commands:
    <<: *rhel-commands

    postinstall:
      # There is no `bwrap`, so disable buildbox-run-bubblewrap appropriately.
      - rm /usr/local/bin/buildbox-run

  dependencies:
    <<: *rhel-dependencies

    buildstream:
      - fuse3

    ostree: []
